package com.krishmasand.basketballtracker;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.HashSet;

public class SummaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !actionBar.isShowing()) {
            actionBar.show();
        }

        Intent intent = getIntent();
        int ascore = (int) intent.getSerializableExtra("ascore");
        int bscore = (int) intent.getSerializableExtra("bscore");
        int fthit = (int) intent.getSerializableExtra("fthit");
        int ftmiss = (int) intent.getSerializableExtra("ftmiss");
        int hit2 = (int) intent.getSerializableExtra("2phit");
        int miss2 = (int) intent.getSerializableExtra("2pmiss");
        int hit3 = (int) intent.getSerializableExtra("3phit");
        int miss3 = (int) intent.getSerializableExtra("3pmiss");

        TextView totalTV = (TextView) findViewById(R.id.total);
        TextView fgTV = (TextView) findViewById(R.id.fg);
        TextView threeTV = (TextView) findViewById(R.id.tp);
        TextView ftTV = (TextView) findViewById(R.id.ft);
        TextView as = (TextView) findViewById(R.id.ascore);
        TextView bs = (TextView) findViewById(R.id.bscore);

        String _totalpoints = "" + (2 * hit2 + 3 * hit3 + fthit);
        String _fg = "" + (hit2+hit3) + "-" + (hit2+miss2+hit3+miss3);
        String _tp = "" + (hit3) + "-" + (hit3+miss3);
        String _ft = "" + (fthit) + "-" + (fthit+ftmiss);
        String _as = "" + ascore;
        String _bs = "" + bscore;

        totalTV.setText(_totalpoints);
        fgTV.setText(_fg);
        threeTV.setText(_tp);
        ftTV.setText(_ft);
        as.setText(_as);
        bs.setText(_bs);


    //email stuff
        Button email = (Button) findViewById(R.id.email_button);

        final String emailString = "Final Score: Home " + _as + ", Away " + _bs + "\n" +
                "Total Points: " + _totalpoints + "\n" +
                "Field Goals: " + _fg + "\n" +
                "Three Pointers: " + _tp + "\n" +
                "Free Throws: " + _ft + "\n";

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent email = new Intent(Intent.ACTION_SEND);
                //email.putExtra(Intent.EXTRA_EMAIL, new String[]{"youremail@yahoo.com"});
                email.putExtra(Intent.EXTRA_SUBJECT, "Basketball Tracker Results");
                email.putExtra(Intent.EXTRA_TEXT, emailString);
                email.setType("message/rfc822");
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        });



    }

}
