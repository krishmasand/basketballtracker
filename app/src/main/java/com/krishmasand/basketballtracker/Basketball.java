package com.krishmasand.basketballtracker;

import java.io.Serializable;

/**
 * Created by krishmasand on 4/2/16.
 */
public class Basketball implements Serializable {
    public boolean made;
    public int points;

    // for serializability purposes
    public Basketball() {}

    // free throw constructor
    public Basketball(boolean _made) {
        made = _made;
        points = 1;
    }
 
    // field goal constructor
    public Basketball(double x, double y, boolean _made) {
        made = _made;
        points = calculateScore(x, y);
    }

    public static int calculateScore(double scaled_x, double scaled_y) {
        int score = 2;
        
        double y_straight_three_len = 170.0;

        if (scaled_y < y_straight_three_len) {
            double x_left_three = 40.00;
            double x_right_three = 560.0;
            if (scaled_x < x_left_three || scaled_x > x_right_three) {
                score = 3;
            }
        } else {
            // double basket_from_baseline = 66.0;
            double y_from_basket = scaled_y - 66.0;
            double x_from_basket = scaled_x - 300.0;
            // double top_of_arc_three = 347.0;
            double R_2 = 78961.0; // (347.0 - 66.0)^2
            if (y_from_basket * y_from_basket + x_from_basket * x_from_basket > R_2) {
                score = 3;
            }
        }

        return score;
    }
}
