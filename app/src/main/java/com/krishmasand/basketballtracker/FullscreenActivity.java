package com.krishmasand.basketballtracker;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {
    //represents the most recent spot created

    static int ascore = 0;
    static int bscore = 0;
    static int fthit = 0;
    static int ftmiss = 0;
    static int hit2 = 0;
    static int miss2 = 0;
    static int hit3 = 0;
    static int miss3 = 0;

    static int x;
    static int y;

    static long timer = 0;

    class ballPair {
        Basketball ball;
        ImageView iv;
        public ballPair(Basketball _ball, ImageView _iv){
            ball = _ball;
            iv = _iv;
        }
    }
 
    // data structure for history
    static final Stack<ballPair> history = new Stack<>();

    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    private void save() {
        try {
            File path=new File(getFilesDir(),"myfolder");
            path.mkdirs();
            File mypath=new File(path,"myfile.txt");
            String s = "" + ascore + "," + bscore + "," + fthit + "," + ftmiss + "," +
                    hit2 + "," + miss2 + "," + hit3 + "," + miss3;
            BufferedWriter b = new BufferedWriter(new FileWriter(mypath));
            b.write(s);
            b.close();
            Log.wtf("wtf", fileList()[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // set landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_fullscreen);

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        // mContentView = findViewById(R.id.fullscreen_content);
        mContentView = findViewById(R.id.rl1);

        // adapt the image to the size of the display
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int dX = size.x;
        final int dY = size.y;
        Drawable d = getResources().getDrawable(R.drawable.nba_court);

        double prop = (double) d.getIntrinsicHeight() / (double) size.y;
        int newX = (int) ( (double) d.getIntrinsicWidth() / prop);

        Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                getResources(), R.drawable.nba_court2), newX, dY, true);

        // finds x-coordinate beginning and ending of court so can't spawn balls outside of the court
        final int xBegin = (int) ((dX / 2.0) - (newX / 2.0));
        final int xEnd = (int) ((dX / 2.0) + (newX / 2.0));

        // fill the background ImageView with the resized image
        ImageView iv_background = (ImageView) findViewById(R.id.im1);
        iv_background.setImageBitmap(bmp);

        // handles basketball spawning and score editing
        final RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl1);
        rl.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View v, MotionEvent event) {
                int action = event.getActionMasked();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:

                        // gets coordinates
                        x = (int) event.getX();
                        y = (int) event.getY();
                }
                return false;
            }
        });

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if inside court, do stuff
                if (x <= xEnd && x >= xBegin && y >= 0) {
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    final ImageView iv = new ImageView(getApplicationContext());

                    iv.setLayoutParams(lp);

                    Drawable d = getResources().getDrawable(R.drawable.basketball);

                    iv.setImageDrawable(d);

                    // sets where to put basketball
                    lp.setMargins(x - (d.getIntrinsicWidth() / 2), y - (d.getIntrinsicHeight() / 2), 0, 0);

                    // places basketball image on screen
                    ((ViewGroup) v).addView(iv);

                    // i want to scale [xBegin, xEnd] to [0, 600]
                    double court_width = xEnd - xBegin;
                    double scale_width = 600.0;
                    double scaled_x = ((double) (x - xBegin)) * scale_width / court_width;
                    double scaled_y = ((double) (y)) * scale_width / court_width;

                    // creates new basketball object for the hashmap
                    Log.d("coordinates", x + " x " + y);

                    Basketball b = new Basketball(scaled_x, scaled_y, true);

                    if (b.points == 2) {
                        if (b.made) {
                            hit2 += 1;
                        } else {
                            miss2 += 1;
                        }
                    } else {
                        if (b.made) {
                            hit3 += 1;
                        } else {
                            miss3 += 1;
                        }
                    }

                    ballPair to_push = new ballPair(b, iv);
                    history.push(to_push);
                    save();
                }
            }
        });

        rl.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // if inside court, do stuff
                if (x <= xEnd && x >= xBegin && y >= 0) {
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    final ImageView iv = new ImageView(getApplicationContext());

                    iv.setLayoutParams(lp);

                    Drawable d = getResources().getDrawable(R.drawable.blue_basketball);

                    iv.setImageDrawable(d);

                    // sets where to put basketball
                    lp.setMargins(x - (d.getIntrinsicWidth() / 2), y - (d.getIntrinsicHeight() / 2), 0, 0);

                    // places basketball image on screen
                    ((ViewGroup) v).addView(iv);

                    // i want to scale [xBegin, xEnd] to [0, 600]
                    double court_width = xEnd - xBegin;
                    double scale_width = 600.0;
                    double scaled_x = ((double) (x - xBegin)) * scale_width / court_width;
                    double scaled_y = ((double) (y)) * scale_width / court_width;

                    // creates new basketball object for the hashmap
                    Log.d("coordinates", x + " x " + y);

                    Basketball b = new Basketball(scaled_x, scaled_y, false);

                    if (b.points == 2) {
                        if (b.made) {
                            hit2+=1;
                        } else {
                            miss2+=1;
                        }
                    } else {
                        if (b.made) {
                            hit3+=1;
                        } else {
                            miss3+=1;
                        }
                    }

                    ballPair to_push = new ballPair(b, iv);
                    history.push(to_push);
                    save();
                }
                return true;
            }

        });

        // goes to summary screen, passing along the stats in the intent
        Button done = (Button) findViewById(R.id.done_button);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SummaryActivity.class);
                intent.putExtra("ascore", ascore);
                intent.putExtra("bscore", bscore);
                intent.putExtra("fthit", fthit);
                intent.putExtra("ftmiss", ftmiss);
                intent.putExtra("2phit", hit2);
                intent.putExtra("2pmiss", miss2);
                intent.putExtra("3phit", hit3);
                intent.putExtra("3pmiss", miss3);
                startActivity(intent);
            }
        });

        Button aup = (Button) findViewById(R.id.aup_button);
        aup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ascore += 1;
                ((TextView) findViewById(R.id.ascore)).setText("HOME: " + ascore);
            }
        });

        Button adown = (Button) findViewById(R.id.adown_button);
        adown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ascore == 0) {
                    return;
                }
                ascore -= 1;
                ((TextView)findViewById(R.id.ascore)).setText("HOME: " + ascore);
            }
        });

        Button bup = (Button) findViewById(R.id.bup_button);
        bup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bscore += 1;
                ((TextView)findViewById(R.id.bscore)).setText("AWAY: " + bscore);
            }
        });

        Button bdown = (Button) findViewById(R.id.bdown_button);
        bdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bscore == 0) {
                    return;
                }
                bscore -= 1;
                ((TextView)findViewById(R.id.bscore)).setText("AWAY: " + bscore);
            }
        });

        final Button fth = (Button) findViewById(R.id.fthit_button);
        fth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fthit += 1;
                Basketball b = new Basketball(true);
                history.push(new ballPair(b, null));
                save();
                ((TextView) findViewById(R.id.fttext)).setText("FREE THROWS: " + fthit + "/" + (fthit + ftmiss));
            }
        });

        Button ftm = (Button) findViewById(R.id.ftmiss_button);
        ftm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ftmiss += 1;
                Basketball b = new Basketball(false);
                history.push(new ballPair(b, null));
                save();
                ((TextView) findViewById(R.id.fttext)).setText("FREE THROWS: " + fthit + "/" + (fthit + ftmiss));
            }
        });

        Button undo = (Button) findViewById(R.id.undo_button);
        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (history.empty()) {
                    return;
                }
                ballPair last = history.pop();
                save();
                // remove the ball from the screen
                ((ViewGroup) rl).removeView(last.iv);

                // update the globals
                Basketball ball = last.ball;
                switch (ball.points) {
                    case 1:
                        if (ball.made) {
                            fthit -= 1;
                        } else {
                            ftmiss -= 1;
                        }
                        ((TextView) findViewById(R.id.fttext)).setText("FREE THROWS: " + fthit + "/" + (fthit + ftmiss));
                        break;
                    case 2:
                        if (ball.made) {
                            hit2 -= 1;
                        } else {
                            miss2 -= 1;
                        }
                        break;
                    case 3:
                        if (ball.made) {
                            hit3 -= 1;
                        } else {
                            miss3 -= 1;
                        }
                        break;
                }
            }
        });

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("");
        alert.setMessage("Welcome to Hoop-Tracker!\n\n" +
                "Click to record made shot.\n\n" +
                        "Long click to record missed shot.\n\n" +
                        "Record free throws at bottom.");

        alert.setPositiveButton("Start!", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        //delayedHide(100);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        //delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.hide();
//        }
//        mControlsView.setVisibility(View.GONE);
//        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
//        mHideHandler.removeCallbacks(mShowPart2Runnable);
//        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

}
